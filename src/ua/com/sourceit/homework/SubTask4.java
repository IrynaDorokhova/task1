package ua.com.sourceit.homework;

import java.util.Scanner;

public class SubTask4 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int value = Integer.parseInt(args[0]);
        int sum = 0;

        for(; value != 0; value /= 10){
            sum += value % 10;
        }
        System.out.printf("%d\n", sum);
        System.out.println();
    }
}
