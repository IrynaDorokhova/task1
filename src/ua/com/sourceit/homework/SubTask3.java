package ua.com.sourceit.homework;

import java.util.Scanner;

public class SubTask3 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);

        while (number1 != number2)
        {
            if(number1 > number2){
                number1 = number1 - number2;
            }else {
                number2 = number2 - number1;
            }
        }int result = number1;
        System.out.println(result);
        System.out.println();
    }
}
