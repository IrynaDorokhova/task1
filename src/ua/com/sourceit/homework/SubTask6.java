package ua.com.sourceit.homework;

import java.util.Scanner;

public class SubTask6 {

        public static int getNumber(String name) {
        int number =0;
        for(int i = 0; i < name.length(); i++){
            number *= 26;
            number += name.toUpperCase().charAt(i) - 'A' + 1;
        }
        return number;

        }
        public static String getName(int number){
            StringBuilder sb = new StringBuilder();
            while (number-- > 0){
                sb.append((char)('A' + (number % 26)));
                number /= 26;
            }
            return sb.reverse().toString();
        }
        public static int next(String name){
            return getNumber(name) + 1;
        }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
            int number = sc.nextInt();
            String name = sc.nextLine();
            String nextNumber = sc.nextLine();
        System.out.println(getName(number));
        System.out.println(next(nextNumber) - 1);
        System.out.println(next(nextNumber));
    }

}
