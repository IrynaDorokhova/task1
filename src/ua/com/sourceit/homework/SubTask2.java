package ua.com.sourceit.homework;

import java.util.Scanner;

public class SubTask2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        StringBuilder information = new StringBuilder();
        for(String s : args){
            information = information.append(s).append(" ");
        }
        System.out.println(information.toString().trim());
    }
}
