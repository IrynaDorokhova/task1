package ua.com.sourceit.homework;

import java.util.ArrayList;

public class Test {
    public static String convertSimbol(char simbol){
        Character ch = 'A';
        System.out.println(ch.toString());
        return numToStr(2);
    }

    public static String convert(int number) {

        StringBuilder result = new StringBuilder();
        int delimoe = number;
        while (true) {
            int ostatok = delimoe % 26;
            result.append(numToStr(ostatok));
            delimoe = delimoe / 26;
            if (ostatok == 0) delimoe = delimoe - 1;

            if (delimoe < 26) result.append(numToStr(delimoe));

            if (delimoe < 26) break;
        }

        return result.reverse().toString();
    }

    private static String numToStr(int a) {
        switch (a) {
            case 1:
                return "A";
            case 2:
                return "B";
            case 3:
                return "C";
            case 4:
                return "D";
            case 5:
                return "E";
            case 6:
                return "F";
            case 7:
                return "G";
            case 8:
                return "H";
            case 9:
                return "I";
            case 10:
                return "J";
            case 11:
                return "K";
            case 12:
                return "L";
            case 13:
                return "M";
            case 14:
                return "N";
            case 15:
                return "O";
            case 16:
                return "P";
            case 17:
                return "Q";
            case 18:
                return "R";
            case 19:
                return "S";
            case 20:
                return "T";
            case 21:
                return "U";
            case 22:
                return "V";
            case 23:
                return "W";
            case 24:
                return "X";
            case 25:
                return "Y";
            case 26:
                return "Z";
            case 0:
                return "";
            default:
                return "";

        }
    }
    public static void main(String[] args) {
        System.out.println(convert(Integer.parseInt(args[0])));
        System.out.println(convertSimbol('A'));
    }

}
