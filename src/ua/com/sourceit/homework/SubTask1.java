package ua.com.sourceit.homework;

import java.util.Locale;
import java.util.Scanner;

public class SubTask1 {
    public static void main(String[] args){
        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        double number1 = Double.parseDouble(args[0]);
        double number2 = Double.parseDouble(args[1]);

        double number3 = number1 + number2;
        System.out.printf("%.1f", number3);
        System.out.println();

    }
}
